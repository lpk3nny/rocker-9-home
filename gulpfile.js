var gulp = require('gulp');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var postcss = require('gulp-postcss');

var jslibs = [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/underscore/underscore-min.js',
    './node_modules/backbone/backbone.js'
];

gulp.task('watch', function () {
    gulp.watch(['./less/init.less', './less/**/*.less'], ['less']);
});

gulp.task('less', function () {
    return gulp.src('./less/app.less')
        .pipe(sourcemaps.init())
        .pipe(less().on('error', function (e) {
            console.error(e);

            this.emit('end');
        }))
        .pipe(postcss([require('postcss-inline-svg')]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css/'));
});

gulp.task('compress', function () {
    return gulp.src(jslibs)
        .pipe(uglify())
        .pipe(gulp.dest('./build/js/'));
});